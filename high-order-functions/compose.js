'use strict';

const reverse = (fns) => reverseInner(fns, []);
const reverseInner = (fns, result) => fns.length === 0 ? result : reverseInner(fns.slice(1), [fns[0], ...result]);

const compose = (...fns) => (...args) => fns.length === 0 ? undefined : composeInner(reverse(fns).slice(1), reverse(fns)[0](...args));
const composeInner = (fns, result) => fns.length === 0 ? result : composeInner(fns.slice(1), fns[0](result));

module.exports = compose;