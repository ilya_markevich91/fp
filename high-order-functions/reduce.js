'use strict';

const reduce = (fn, list, accumulator) => list.length === 0 ? accumulator : reduce(fn, list.slice(1), fn(accumulator, list[0]));

module.exports = reduce;