'use strict';

const map = (fn, values) => mapInner([], fn, values);
const mapInner = (result, fn, values) => values.length === 0 ? result : mapInner(result.concat(fn(values[0])), fn, values.slice(1));

module.exports = map;