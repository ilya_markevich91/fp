'use strict';

const memoize = (fn) => {
    const cache = {};

    return (...args) => {
        const argsKey = JSON.stringify(args);

        if (argsKey in cache) {
            return cache[argsKey];
        }

        const result = fn(...args);

        cache[argsKey] = result;
        return result;
    };
};

module.exports = memoize;