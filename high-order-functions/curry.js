'use strict';

const curry = (fn) => {
    const argsCount = fn.length;
    const args = [];

    return function curryInner(...passedArgs) {
        args.push(...passedArgs);
        return args.length >= argsCount ? fn(...args) : curryInner;
    };
};

module.exports = curry;