'use strict';

const map = require('../map');
const curry = require('../curry');
const partial = require('../partial');
const filter = require('../filter');
const compose = require('../compose');
const construct = require('../construct');
const reduce = require('../reduce');
const cond = require('../cond');
const memoize = require('../memoize');

describe('High order functions', () => {
    describe('map', () => {
        it('should map empty array to empty array', () => {
            expect(map(v => v, [])).toEqual([]);
        });

        it('should map non empty array', () => {
            expect(map(v => v + 1, [1, 2, 3, 4])).toEqual([2, 3, 4, 5]);
        });
    });

    describe('curry', () => {
        it('curry fn without args', () => {
            const fn = curry(() => 'test');

            expect(fn()).toEqual('test');
        });

        it('curry binary fn', () => {
            const fn = curry((a, b) => a + b);

            expect(fn(2)(8)).toEqual(10);
            expect(fn(2, 8)).toEqual(10);
        });
    });

    describe('partial', () => {
        it('partial fn without args', () => {
            const fn = partial(() => 'test');

            expect(fn()).toEqual('test');
        });

        it('partial fn with 3 args', () => {
            const fn = partial((a, b, c) => a + b + c, 1, 2);

            expect(fn(3)).toEqual(6);
        });
    });

    describe('filter', () => {
        it('should filter empty array to empty array', () => {
            expect(filter(() => true, [])).toEqual([]);
        });

        it('should filter non empty array', () => {
            expect(filter(v => v % 2 === 0, [1, 2, 3, 4])).toEqual([2, 4]);
        });
    });

    describe('compose', () => {
        it('should return nothing if functions are not specified', () => {
            expect(compose()('test')).toEqual(undefined);
        });

        it('should compose from one function', () => {
            expect(compose((v) => v)('test')).toEqual('test');
        });

        it('should compose more than one function', () => {
            expect(compose((v) => `test ${v}`, (v) => v / 2, (v) => v + 1)(7)).toEqual('test 4');
        });
    });

    describe('construct', () => {
        it('should return nothing if functions are not specified', () => {
            expect(construct([], [])).toEqual([]);
        });

        it('should apply tuple to array of functions', () => {
            expect(construct(
                [
                    (args) => args.reduce((result, value) => result + value, 0),
                    (args) => args.reduce((result, value) => result * value, 1),
                    (args) => args.reduce((result, value) => result - value, 0)
                ],
                [1, 2, 3])
            ).toEqual([6, 6, -6]);
        });
    });

    describe('reduce', () => {
        it('should return predefined accumulator for empty list', () => {
            expect(reduce((a, v) => v, [], 0)).toEqual(0);
        });

        it('should return reduced value for non empty list', () => {
            expect(reduce((a, v) => a + v, [1, 2, 3], 0)).toEqual(6);
        });
    });

    describe('cond', () => {
        it('should return first function result because predicate is true', () => {
            expect(cond(
                (v) => v % 2 === 0,
                (v) => v * v,
                (v) => v * v * v,
            )(4)).toEqual(16);
        });

        it('should return second function result because predicate is false', () => {
            expect(cond(
                (v) => v % 2 === 0,
                (v) => v * v,
                (v) => v * v * v,
            )(3)).toEqual(27);
        });
    });

    describe('memoize', () => {
        it('should memoize result', () => {
            const args = [1, 2];
            const fn = jest.fn().mockReturnValueOnce(10);
            const memoizedFn = memoize(fn);

            expect(memoizedFn(...args)).toEqual(10);
            expect(memoizedFn(...args)).toEqual(10);

            expect(fn).toBeCalledWith(...args);
            expect(fn.mock.calls.length).toEqual(1);
        });
    });
});