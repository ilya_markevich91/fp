'use strict';

const cond = (predicate, fn1, fn2) => (...args) => predicate(...args) ? fn1(...args) : fn2(...args);

module.exports = cond;