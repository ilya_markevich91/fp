'use strict';

const EMPTY_TUPLE = [];

const construct = (fns, tuple) => fns.length === 0 ? EMPTY_TUPLE : constructInner(fns, tuple, EMPTY_TUPLE);
const constructInner = (fns, tuple, resultTuple) =>
    fns.length === 0
        ? resultTuple
        : constructInner(fns.slice(1), tuple, resultTuple.concat(fns[0](tuple)));

module.exports = construct;