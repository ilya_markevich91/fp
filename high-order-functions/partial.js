'use strict';

const partial = (fn, ...args) => (...otherArgs) => fn(...args.concat(otherArgs));

module.exports = partial;