'use strict';

const filter = (fn, values) => filterInner([], fn, values);
const filterInner = (result, fn, values) => values.length === 0
    ? result
    : filterInner(result.concat(fn(values[0]) ? values[0] : []), fn, values.slice(1));

module.exports = filter;