'use strict';

const EMPTY = undefined;

const createNode = (next, value) => ({ next, value });
const append = (set, value) => createNode(set, value);
const first = (set) => set === EMPTY ? EMPTY : set.value;
const rest = (set) => set.next;

const add = (set, value) => has(set, value) ? set : append(set, value);
const has = (set, value) =>
    isEmpty(set) ? false
        : first(set) === value ? true
            : has(rest(set), value);
const isEmpty = (set) => set === EMPTY;

const length = (set) => lengthInner(set, 0);
const lengthInner = (set, length) => isEmpty(set) ? length : lengthInner(rest(set), length + 1);

const create = (...values) => createInner(EMPTY, values);
const createInner = (set, values) => values.length === 0 ? set : createInner(add(set, values[0]), values.slice(1));

const forEach = (set, fn) => {
    if (!isEmpty(set)) {
        fn(first(set));
        forEach(rest(set), fn);
    }
};

const filter = (set, fn) => filterInner(EMPTY, set, fn);
const filterInner = (resultSet, set, fn) =>
    isEmpty(set) ? resultSet : filterInner(fn(first(set)) ? add(resultSet, first(set)) : resultSet, rest(set), fn);

const isEqual = (set1, set2) =>
    isEmpty(set1) && isEmpty(set2) ? true
        : !has(set2, first(set1)) ? false
            : isEqual(rest(set1), remove(set2, first(set1)));

const map = (set, fn) => mapInner(EMPTY, set, fn);
const mapInner = (resultSet, set, fn) => isEmpty(set) ? resultSet : mapInner(add(resultSet, fn(first(set))), rest(set), fn);

const difference = (set1, set2) => differenceInner(EMPTY, set1, set2);
const differenceInner = (resultSet, set1, set2) =>
    isEmpty(set1) ? union(resultSet, set2)
        : differenceInner(!has(set2, first(set1)) ? add(resultSet, first(set1)) : resultSet, rest(set1), remove(set2, set1.value));

const intersection = (set1, set2) => intersectionInner(EMPTY, set1, set2);
const intersectionInner = (resultSet, set1, set2) =>
    isEmpty(set1) ? resultSet : intersectionInner(has(set2, first(set1)) ? add(resultSet, first(set1)) : resultSet, rest(set1), set2);

const union = (set1, set2) => isEmpty(set1) ? set2 : union(rest(set1), add(set2, first(set1)));

const remove = (set, value) => filter(set, (v) => v !== value);

const reduce = (set, fn) => isEmpty(set) ? EMPTY : reduceInner(first(set), rest(set), fn);
const reduceInner = (result, set, fn) => isEmpty(set) ? result : reduceInner(fn(result, first(set)), rest(set), fn);

module.exports = {
    EMPTY,
    add,
    has,
    isEmpty,
    length,
    create,
    forEach,
    filter,
    isEqual,
    map,
    difference,
    intersection,
    union,
    remove,
    reduce
};