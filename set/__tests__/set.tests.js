'use strict';

/* eslint-disable max-statements */

const {
    EMPTY, create, isEmpty, add, has, length, union, map, isEqual,
    filter, forEach, remove, difference, intersection, reduce
} = require('../set');

const checkSetValues = (set, values) => {
    let iterationHead = set;
    const iterationValues = values.slice(0);

    while (iterationHead !== EMPTY) {
        const valueIndex = iterationValues.indexOf(iterationHead.value);

        expect(valueIndex).not.toEqual(-1);
        iterationValues.splice(valueIndex, 1);

        iterationHead = iterationHead.next;
    }

    expect(iterationValues.length).toEqual(0);
};

describe('Set', () => {
    describe('#create', () => {
        it('should make empty set', () => {
            const set = create();

            expect(set).toEqual(EMPTY);
        });

        it('should make set with predefined values', () => {
            const values = [1, 2, 3, 4, 3, 2, 1];
            const set = create(...values);

            checkSetValues(set, [1, 2, 3, 4]);
        });
    });

    describe('#isEmpty', () => {
        it('should return true for empty set', () => {
            const set = create();

            expect(isEmpty(set)).toEqual(true);
        });

        it('should return false for non empty set', () => {
            const set = create(1, 2, 3, 4);

            expect(isEmpty(set)).toEqual(false);
        });
    });

    describe('#add', () => {
        it('should return set with added element for empty set', () => {
            const set = create();

            checkSetValues(add(set, 1), [1]);
        });

        it('should return set with element and set content for non empty set', () => {
            const set = create(1, 2, 3, 4);

            checkSetValues(add(set, 0), [0, 1, 2, 3, 4]);
        });

        it('should not insert element which is already in set', () => {
            const set = create(1, 2, 3, 4);

            checkSetValues(add(set, 1), [1, 2, 3, 4]);
        });
    });

    describe('#has', () => {
        it('should return true if set has element', () => {
            const set = create(1, 2);

            expect(has(set, 1)).toEqual(true);
        });

        it('should return false if set not has element', () => {
            const set = create(1, 2, 3, 4);

            expect(has(set, 5)).toEqual(false);
        });
    });

    describe('#length', () => {
        it('should return 0 for empty set', () => {
            const set = create();

            expect(length(set)).toEqual(0);
        });

        it('should return actual non empty set length', () => {
            const set = create(1, 2, 3, 4);

            expect(length(set)).toEqual(4);
        });
    });

    describe('#union', () => {
        it('should union two empty sets', () => {
            expect(union(create(), create())).toEqual(EMPTY);
        });

        it('should union empty and non empty sets', () => {
            const set = create(1, 2, 3, 4);
            const emptySet = create();

            checkSetValues(union(emptySet, set), [1, 2, 3, 4]);
            checkSetValues(union(set, emptySet), [1, 2, 3, 4]);
        });

        it('should concat two non empty sets', () => {
            const set1 = create(1, 2, 3, 4);
            const set2 = create(4, 5, 6);

            checkSetValues(union(set1, set2), [1, 2, 3, 4, 5, 6]);
        });
    });

    describe('#map', () => {
        it('should map empty set', () => {
            const emptySet = create();

            expect(map(emptySet, (v) => v)).toEqual(EMPTY);
        });

        it('should map non empty set', () => {
            const set = create(1, 2, 3, 4);

            checkSetValues(map(set, v => v % 2 === 0), [true, false]);
        });
    });

    describe('#reduce', () => {
        it('should reduce empty set', () => {
            const emptySet = create();

            expect(reduce(emptySet, (v) => v)).toEqual(EMPTY);
        });

        it('should reduce non empty set', () => {
            const emptySet = create(1, 2, 3, 4);

            expect(reduce(emptySet, (result, value) => result + value)).toEqual(10);
        });
    });

    describe('#isEqual', () => {
        it('should return true for two empty sets', () => {
            expect(isEqual(create(), create())).toEqual(true);
        });

        it('should return false if one of sets is empty', () => {
            expect(isEqual(create(), create(1, 2, 3, 4))).toEqual(false);
            expect(isEqual(create(1, 2, 3, 4), create())).toEqual(false);
        });

        it('should return true if sets have same elements', () => {
            expect(isEqual(create(1, 2, 3, 4), create(1, 2, 3, 4))).toEqual(true);
        });

        it('should return true if sets have same elements in different order', () => {
            expect(isEqual(create(4, 3, 2, 1), create(1, 2, 3, 4))).toEqual(true);
        });

        it('should return false if sets have different length', () => {
            expect(isEqual(create(1, 2, 3), create(1, 2, 3, 4))).toEqual(false);
        });
    });

    describe('#filter', () => {
        it('should filter empty set', () => {
            expect(filter(create(), v => v)).toEqual(EMPTY);
        });

        it('should filter one element in set with one element', () => {
            expect(filter(create(1), v => v !== 1)).toEqual(EMPTY);
        });

        it('should filter some elements from set', () => {
            checkSetValues(filter(create(1, 2, 3, 4), v => v % 2 === 0), [2, 4]);
        });
    });

    describe('#forEach', () => {
        it('should iterate on empty set', () => {
            const fn = jest.fn();

            forEach(create(), fn);
            expect(fn).not.toHaveBeenCalled();
        });

        it('should iterate on non empty set', () => {
            const fn = jest.fn();
            const set = create(1, 2, 3, 4);

            forEach(set, fn);
            checkSetValues(set, fn.mock.calls.map(([arg]) => arg));
        });
    });

    describe('#remove', () => {
        it('should do nothing on empty set', () => {
            const set = create();

            checkSetValues(remove(set, 1), []);
        });

        it('should remove element from non empty set', () => {
            const set = create(1, 2, 3, 4);

            checkSetValues(remove(set, 2), [1, 3, 4]);
        });
    });

    describe('#difference', () => {
        it('should return difference as empty set for two empty sets', () => {
            expect(difference(create(), create())).toEqual(EMPTY);
        });

        it('should return difference as non empty set for empty and non empty sets', () => {
            checkSetValues(difference(create(1, 2, 3, 4), create()), [1, 2, 3, 4]);
            checkSetValues(difference(create(), create(1, 2, 3, 4)), [1, 2, 3, 4]);
        });

        it('should return difference between two non empty sets', () => {
            checkSetValues(difference(create(1, 2, 3, 4), create(2, 4, 5, 6)), [1, 3, 5, 6]);
        });
    });

    describe('#intersection', () => {
        it('should return intersection as empty set for two empty sets', () => {
            expect(intersection(create(), create())).toEqual(EMPTY);
        });

        it('should return difference as empty set for empty and non empty sets', () => {
            expect(intersection(create(1, 2, 3, 4), create())).toEqual(EMPTY);
            expect(intersection(create(), create(1, 2, 3, 4))).toEqual(EMPTY);
        });

        it('should return intersection between two non empty sets', () => {
            checkSetValues(intersection(create(1, 2, 3, 4), create(2, 4, 5, 6)), [2, 4]);
        });
    });
});