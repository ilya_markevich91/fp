'use strict';

const {
    EMPTY, create, isEmpty, has, set, get, isEqual,
    forEach, values, keys, remove
} = require('../map');

const checkMapValues = (map, values) => {
    let iterationHead = map;
    const iterationValues = values.slice(0);

    while (iterationHead !== EMPTY) {
        // eslint-disable-next-line no-loop-func
        const valueIndex = iterationValues.findIndex(([key, value]) => iterationHead.value === value && iterationHead.key === key);

        expect(valueIndex).not.toEqual(-1);
        iterationValues.splice(valueIndex, 1);

        iterationHead = iterationHead.next;
    }

    expect(iterationValues.length).toEqual(0);
};

describe('Map', () => {
    describe('#create', () => {
        it('should make empty map', () => {
            const map = create();

            expect(map).toEqual(EMPTY);
        });

        it('should make map with predefined values', () => {
            const values = [[1, 1], [2, 3], [4, 4]];
            const map = create(...values);

            checkMapValues(map, [[1, 1], [2, 3], [4, 4]]);
        });

        it('should make map with predefined values which have duplicate keys', () => {
            const values = [[1, 1], [2, 3], [2, 4], [4, 4]];
            const map = create(...values);

            checkMapValues(map, [[1, 1], [2, 4], [4, 4]]);
        });
    });

    describe('#isEmpty', () => {
        it('should return true for empty map', () => {
            const map = create();

            expect(isEmpty(map)).toEqual(true);
        });

        it('should return false for non empty map', () => {
            const map = create([[1, 2], [3, 4]]);

            expect(isEmpty(map)).toEqual(false);
        });
    });

    describe('#set', () => {
        it('should return map with added element for empty map', () => {
            const map = create();

            checkMapValues(set(map, 1, 1), [[1, 1]]);
        });

        it('should return map with element and map content for non empty map', () => {
            const map = create([1, 2], [3, 4]);

            checkMapValues(set(map, 0, 0), [[0, 0], [1, 2], [3, 4]]);
        });

        it('should update element which is already in map', () => {
            const map = create([1, 2], [3, 4]);

            checkMapValues(set(map, 1, 3), [[1, 3], [3, 4]]);
        });
    });

    describe('#get', () => {
        it('should return undefined if map not has element', () => {
            const map = create([1, 2]);

            expect(get(map, 2)).toEqual(undefined);
        });

        it('should return value if map has key', () => {
            const map = create([1, 2]);

            expect(get(map, 1)).toEqual(2);
        });
    });

    describe('#has', () => {
        it('should return true if map has element', () => {
            const map = create([1, 2]);

            expect(has(map, 1)).toEqual(true);
        });

        it('should return false if map not has element', () => {
            const map = create([1, 2]);

            expect(has(map, 5)).toEqual(false);
        });
    });

    describe('#isEqual', () => {
        it('should return true for two empty maps', () => {
            expect(isEqual(create(), create())).toEqual(true);
        });

        it('should return false if one of maps is empty', () => {
            expect(isEqual(create(), create([1, 2], [3, 4]))).toEqual(false);
            expect(isEqual(create([1, 2], [3, 4]), create())).toEqual(false);
        });

        it('should return true if maps have same elements', () => {
            expect(isEqual(create([1, 2], [3, 4]), create([1, 2], [3, 4]))).toEqual(true);
        });

        it('should return true if maps have same elements in different order', () => {
            expect(isEqual(create([4, 3], [2, 1]), create([2, 1], [4, 3]))).toEqual(true);
        });

        it('should return false if maps have same elements but with different value', () => {
            expect(isEqual(create([1, 2], [3, 4]), create([1, 2], [3, 5]))).toEqual(false);
        });

        it('should return false if maps have different length', () => {
            expect(isEqual(create([1, 2]), create([1, 2], [3, 4]))).toEqual(false);
        });
    });

    describe('#forEach', () => {
        it('should iterate on empty map', () => {
            const fn = jest.fn();

            forEach(create(), fn);
            expect(fn).not.toHaveBeenCalled();
        });

        it('should iterate on non empty map', () => {
            const fn = jest.fn();
            const map = create([1, 2], [3, 4]);

            forEach(map, fn);
            checkMapValues(map, fn.mock.calls.map((arg) => arg));
        });
    });

    describe('#remove', () => {
        it('should do nothing on empty map', () => {
            const map = create();

            checkMapValues(remove(map, 1), []);
        });

        it('should remove element from non empty map', () => {
            const map = create([1, 2], [3, 4], [5, 6]);

            checkMapValues(remove(map, 3), [[1, 2], [5, 6]]);
        });
    });

    describe('#values', () => {
        it('should return empty list of values for empty map', () => {
            expect(values(create())).toEqual([]);
        });

        it('should return list of values for non empty map', () => {
            expect(values(create([1, 2], [3, 4]))).toEqual([4, 2]);
        });
    });

    describe('#keys', () => {
        it('should return empty list of keys for empty map', () => {
            expect(keys(create())).toEqual([]);
        });

        it('should return list of keys for non empty map', () => {
            expect(keys(create([1, 2], [3, 4]))).toEqual([3, 1]);
        });
    });
});