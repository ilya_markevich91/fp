'use strict';

const EMPTY = undefined;

const createNode = (next, key, value) => ({ next, key, value });
const rest = (map) => map.next;
const append = (map, key, value) => createNode(map, key, value);
const getKey = (map) => map.key;
const getValue = (map) => map.value;

const create = (...values) => createInner(EMPTY, values);
const createInner = (map, values) => values.length === 0 ? map : createInner(set(map, values[0][0], values[0][1]), values.slice(1));

const isEmpty = (map) => map === EMPTY;

const get = (map, key) =>
    isEmpty(map) ? undefined
        : getKey(map) === key ? getValue(map)
            : get(rest(map), key);

const set = (map, key, value) => has(map, key) ? updateInner(EMPTY, map, key, value) : append(map, key, value);
const updateInner = (resultMap, map, key, value) =>
    isEmpty(map) ? resultMap
        : getKey(map) === key ? updateInner(append(resultMap, key, value), rest(map), key, value)
            : updateInner(append(resultMap, getKey(map), getValue(map)), rest(map), key, value);

const keys = (map) => keysInner(map, []);
const keysInner = (map, keys) => isEmpty(map) ? keys : keysInner(rest(map), keys.concat(getKey(map)));

const values = (map) => valuesInner(map, []);
const valuesInner = (map, values) => isEmpty(map) ? values : valuesInner(rest(map), values.concat(getValue(map)));

const remove = (map, key) => removeInner(EMPTY, map, key);
const removeInner = (resultMap, map, key) =>
    isEmpty(map) ? resultMap : removeInner(getKey(map) === key ? resultMap : append(resultMap, getKey(map), getValue(map)), rest(map), key);

const has = (map, key) =>
    isEmpty(map) ? false
        : getKey(map) === key ? true
            : has(rest(map), key);

const isEqual = (map1, map2) =>
    isEmpty(map1) && isEmpty(map2) ? true
        : isEmpty(map1) || isEmpty(map2) ? false
            : get(map2, getKey(map1)) !== getValue(map1) ? false
                : isEqual(rest(map1), remove(map2, getKey(map1)));

const forEach = (map, fn) => {
    if (!isEmpty(map)) {
        fn(getKey(map), getValue(map));
        forEach(rest(map), fn);
    }
};

module.exports = {
    EMPTY,
    create,
    isEmpty,
    get,
    has,
    set,
    keys,
    values,
    remove,
    isEqual,
    forEach
};