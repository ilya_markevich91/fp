'use strict';

/* eslint-disable max-statements */

const { has, union, intersection, difference, not, arrayToInfiniteList } = require('../recursive');

describe('Recursive infinite list', () => {
    const emptyList = () => false;
    const evenNumbersList = (value) => value % 2 === 0;
    const positiveNumbersList = (value) => value > 0;

    describe('#has', () => {
        it('empty list has no members', () => {
            expect(has(emptyList, 1)).toEqual(false);
            expect(has(emptyList, 'test')).toEqual(false);
        });

        it('list with even numbers have only even numbers as members', () => {
            expect(has(evenNumbersList, 2)).toEqual(true);
            expect(has(evenNumbersList, 4)).toEqual(true);
            expect(has(evenNumbersList, 5)).toEqual(false);
        });
    });

    describe('#union', () => {
        it('union of even and positive numbers lists', () => {
            const unionOfEvenAndPositive = union(evenNumbersList, positiveNumbersList);

            expect(has(unionOfEvenAndPositive, 2)).toEqual(true);
            expect(has(unionOfEvenAndPositive, 14)).toEqual(true);
            expect(has(unionOfEvenAndPositive, -2)).toEqual(true);
            expect(has(unionOfEvenAndPositive, -3)).toEqual(false);
        });

        it('union of even numbers and empty list is the same as even numbers list', () => {
            const values = [-2, 0, 1, 2, 3, 4];
            const unionOfEvenAndEmpty = union(evenNumbersList, emptyList);

            values.forEach((value) => {
                expect(has(unionOfEvenAndEmpty, value)).toEqual(has(evenNumbersList, value));
            });
        });
    });

    describe('#intersection', () => {
        it('intersection of even and positive numbers lists', () => {
            const intersectionOfEvenAndPositive = intersection(evenNumbersList, positiveNumbersList);

            expect(has(intersectionOfEvenAndPositive, 2)).toEqual(true);
            expect(has(intersectionOfEvenAndPositive, 14)).toEqual(true);
            expect(has(intersectionOfEvenAndPositive, -2)).toEqual(false);
            expect(has(intersectionOfEvenAndPositive, -3)).toEqual(false);
        });

        it('intersection of even numbers and empty list is empty list', () => {
            const values = [-2, 0, 1, 2, 3, 4];
            const intersectionOfEvenAndEmpty = intersection(evenNumbersList, emptyList);

            values.forEach((value) => {
                expect(has(intersectionOfEvenAndEmpty, value)).toEqual(false);
            });
        });
    });

    describe('#difference', () => {
        it('difference of even and positive numbers lists', () => {
            const differenceOfEvenAndPositive = difference(evenNumbersList, positiveNumbersList);

            expect(has(differenceOfEvenAndPositive, 3)).toEqual(false);
            expect(has(differenceOfEvenAndPositive, -2)).toEqual(true);
            expect(has(differenceOfEvenAndPositive, 2)).toEqual(false);
        });

        it('difference of even numbers and empty list is even numbers list', () => {
            const values = [-2, 0, 1, 2, 3, 4];
            const differenceOfEvenAndEmpty = difference(evenNumbersList, emptyList);

            values.forEach((value) => {
                expect(has(differenceOfEvenAndEmpty, value)).toEqual(has(evenNumbersList, value));
            });
        });
    });

    describe('#not', () => {
        it('not of even positive numbers lists is odd numbers list', () => {
            const oddNumbersList = not(evenNumbersList);

            expect(has(oddNumbersList, 2)).toEqual(false);
            expect(has(oddNumbersList, 14)).toEqual(false);
            expect(has(oddNumbersList, 5)).toEqual(true);
            expect(has(oddNumbersList, -3)).toEqual(true);
        });

        it('not of empty list is full list', () => {
            const values = [-2, 0, 1, 2, 3, 4];
            const fullList = not(emptyList);

            values.forEach((value) => {
                expect(has(fullList, value)).toEqual(true);
            });
        });
    });

    describe('#arrayToInfiniteList', () => {
        it('empty array to empty infinite list', () => {
            const emptyListFromArray = arrayToInfiniteList([]);
            const values = [1, 2, 3, 4];

            values.forEach((value) => {
                expect(has(emptyListFromArray, value)).toEqual(false);
            });
        });

        it('numbers array to infinite list', () => {
            const emptyListFromArray = arrayToInfiniteList([1, 2, 3, 4]);

            expect(has(emptyListFromArray, 1)).toEqual(true);
            expect(has(emptyListFromArray, 5)).toEqual(false);
        });
    });
});