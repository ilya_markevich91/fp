'use strict';

/* eslint-disable max-statements */

const { first, rest, prefix, map, all, filter, take } = require('../recursivly-enumerable');

describe('Recursivly enumerable infinite list', () => {
    const positiveNumbersList = (i) => i;
    const evenNumbersList = (i) => 2 * i;

    describe('#first', () => {
        it('should get first element of infinite list', () => {
            expect(first(positiveNumbersList)).toEqual(1);
            expect(first(evenNumbersList)).toEqual(2);
        });
    });

    describe('#rest', () => {
        it('should get second element of list through rest', () => {
            expect(first(rest(positiveNumbersList))).toEqual(2);
            expect(first(rest(evenNumbersList))).toEqual(4);
        });
    });

    describe('#prefix', () => {
        it('should prefix list with new element', () => {
            const newPositiveNumbersList = prefix(positiveNumbersList, 0);
            const newEvenNumbersList = prefix(evenNumbersList, -2);

            expect(first(newPositiveNumbersList)).toEqual(0);
            expect(first(rest(newPositiveNumbersList))).toEqual(1);

            expect(first(newEvenNumbersList)).toEqual(-2);
            expect(first(rest(newEvenNumbersList))).toEqual(2);
        });
    });

    describe('#map', () => {
        it('should map infinite list values', () => {
            const newPositiveNumbersList = map(positiveNumbersList, (v) => v + 4);

            expect(first(newPositiveNumbersList)).toEqual(5);
            expect(first(rest(newPositiveNumbersList))).toEqual(6);
        });
    });

    describe('#all', () => {
        it('should create infinite list with same values', () => {
            const foursList = all(4);

            expect(first(foursList)).toEqual(4);
            expect(first(rest(foursList))).toEqual(4);
        });
    });

    describe('#filter', () => {
        it('should filter elements for infinite list', () => {
            const filteredPositiveNumbersList = filter(positiveNumbersList, (v) => v % 3 === 0);

            expect(first(filteredPositiveNumbersList)).toEqual(3);
            expect(first(rest(filteredPositiveNumbersList))).toEqual(6);
        });
    });

    describe('#take', () => {
        it('should take 4 elements of list', () => {
            expect(take(positiveNumbersList, 4)).toEqual([1, 2, 3, 4]);
            expect(take(evenNumbersList, 4)).toEqual([2, 4, 6, 8]);
        });
    });
});