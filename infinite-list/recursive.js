'use strict';

// list described by predicate * -> Boolean

const has = (list, value) => list(value);
const arrayToInfiniteList = (array) => (value) => array.includes(value);
const union = (list1, list2) => (value) => has(list1, value) || has(list2, value);
const intersection = (list1, list2) => (value) => has(list1, value) && has(list2, value);
const difference = (list1, list2) => (value) => has(list1, value) && !has(list2, value);
const not = (list) => (value) => !has(list, value);

module.exports = {
    has,
    union,
    intersection,
    difference,
    not,
    arrayToInfiniteList
};