'use strict';

// list described by formula number -> A

const first = (list) => list(1);
const rest = (list) => (i) => list(i + 1);
const prefix = (list, value) => (i) => i === 1 ? value : list(i - 1);
const map = (list, f) => (i) => f(list(i));

const filter = (list, f) => (i) => filterInner(list, f, i, 1);
const filterInner = (list, f, i, counter) =>
    f(list(counter)) && i === 1 ? list(counter) : filterInner(list, f, f(list(counter)) ? i - 1 : i, counter + 1);

const all = (value) => () => value;
const take = (list, count) => {
    const result = [];

    for (let i = 1; i <= count; i++) {
        result.push(list(i));
    }

    return result;
};

module.exports = {
    first,
    rest,
    prefix,
    map,
    all,
    filter,
    take
};