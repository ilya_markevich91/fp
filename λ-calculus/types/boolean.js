'use strict';

const TRUE = (x1) => () => x1;
const FALSE = () => (x2) => x2;

const cond = (pred) => (x) => (y) => pred(x)(y);
const not = (x) => cond(x)(FALSE)(TRUE);
const and = (x, y) => cond(x)(y)(FALSE);
const or = (x, y) => cond(x)(TRUE)(y);

module.exports = {
    TRUE,
    FALSE,
    cond,
    not,
    and,
    or
};