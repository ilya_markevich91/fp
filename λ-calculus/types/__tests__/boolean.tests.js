'use strict';

const {
    TRUE, FALSE, and, not, or, cond
} = require('../boolean');

const isTrue = (v) => v(1)(2) === 1;
const isFalse = (v) => !isTrue(v);

describe('Boolean', () => {
    describe('True', () => {
        it('true is a first value of 2', () => {
            expect(isTrue(TRUE)).toEqual(true);
        });
    });

    describe('False', () => {
        it('false is a second value of 2', () => {
            expect(isFalse(FALSE)).toEqual(true);
        });
    });

    describe('and', () => {
        it('check truth table', () => {
            const table = [
                [[TRUE, TRUE], isTrue],
                [[TRUE, FALSE], isFalse],
                [[FALSE, TRUE], isFalse],
                [[FALSE, FALSE], isFalse]
            ];

            table.forEach(([[x1, x2], check]) => {
                expect(check(and(x1, x2))).toEqual(true);
            });
        });
    });

    describe('not', () => {
        it('check truth table', () => {
            const table = [
                [[TRUE], isFalse],
                [[FALSE], isTrue]
            ];

            table.forEach(([[x], check]) => {
                expect(check(not(x))).toEqual(true);
            });
        });
    });

    describe('or', () => {
        it('check truth table', () => {
            const table = [
                [[TRUE, TRUE], isTrue],
                [[TRUE, FALSE], isTrue],
                [[FALSE, TRUE], isTrue],
                [[FALSE, FALSE], isFalse]
            ];

            table.forEach(([[x1, x2], check]) => {
                expect(check(or(x1, x2))).toEqual(true);
            });
        });
    });

    describe('cond', () => {
        it('check true and false conditions', () => {
            const table = [
                [[TRUE, FALSE, TRUE], isFalse],
                [[FALSE, FALSE, TRUE], isTrue]
            ];

            table.forEach(([[pred, x1, x2], check]) => {
                expect(check(cond(pred)(x1)(x2))).toEqual(true);
            });
        });
    });
});