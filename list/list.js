'use strict';

const EMPTY = undefined;

const createNode = (next, value) => ({ next, value });
const getValue = (node) => node === EMPTY ? EMPTY : node.value;

const clone = (list) => cat(EMPTY, list);
const postfix = (list, value) => cat(list, createNode(EMPTY, value));
const isEmpty = (list) => list === EMPTY;
const first = (list) => isEmpty(list) ? EMPTY : list.value;
const rest = (list) => isEmpty(list) ? EMPTY : list.next;
const prefix = (list, value) => createNode(list, value);
const elt = (list, index) => index === 1 ? first(list) : elt(rest(list), index - 1);
const remove = (list, value) => filter(list, (v) => v !== value);

const cat = (list1, list2) => catInner(reverse(list1), list2);
const catInner = (list1, list2) => isEmpty(list1) ? list2 : catInner(rest(list1), prefix(list2, list1.value));

const reverse = (list) => reverseInner(list, EMPTY);
const reverseInner = (sourceList, destinationList) =>
    isEmpty(sourceList) ? destinationList : reverseInner(rest(sourceList), prefix(destinationList, first(sourceList)));

const create = (...values) => createInner(EMPTY, values);
const createInner = (resultList, values) => values.length === 0 ? reverse(resultList) : createInner(prefix(resultList, values[0]), values.slice(1));

const length = (list) => lengthInner(list, 0);
const lengthInner = (list, length) => isEmpty(list) ? length : lengthInner(rest(list), length + 1);

const map = (list, fn) => isEmpty(list) ? EMPTY : mapInner(list, EMPTY, fn);
const mapInner = (sourceList, destinationList, fn) =>
    isEmpty(sourceList) ? reverse(destinationList) : mapInner(rest(sourceList), prefix(destinationList, fn(sourceList.value)), fn);

const reduce = (list, fn) => reduceInner(getValue(list), rest(list), fn);
const reduceInner = (result, list, fn) => isEmpty(list) ? result : reduceInner(fn(result, list.value), rest(list), fn);

const generateNumbers = (lowerBound, upperBound) => generateNumbersInner(EMPTY, lowerBound, upperBound);
const generateNumbersInner = (list, lowerBound, upperBound) =>
    upperBound < lowerBound ? list : generateNumbersInner(prefix(list, upperBound), lowerBound, upperBound - 1);

const isEqual = (list1, list2) =>
    isEmpty(list1) && isEmpty(list2) ? true
        : first(list1) !== first(list2) ? false
            : isEqual(rest(list1), rest(list2));

const filter = (list, fn) => isEmpty(list) ? EMPTY : filterInner(list, EMPTY, fn);
const filterInner = (sourceList, destinationList, fn) =>
    isEmpty(sourceList) ? reverse(destinationList)
        : filterInner(rest(sourceList), fn(sourceList.value) ? prefix(destinationList, sourceList.value) : destinationList, fn);

const forEach = (list, fn) => {
    if (!isEmpty(list)) {
        fn(list.value);
        forEach(rest(list), fn);
    }
};

module.exports = {
    EMPTY,
    create,
    clone,
    isEmpty,
    first,
    rest,
    prefix,
    postfix,
    length,
    elt,
    cat,
    map,
    reduce,
    generateNumbers,
    isEqual,
    reverse,
    filter,
    forEach,
    remove
};

