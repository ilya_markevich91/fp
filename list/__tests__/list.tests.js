'use strict';

/* eslint-disable max-statements */

const {
    EMPTY, create, clone, isEmpty, first, rest, prefix, postfix, length, elt,
    cat, map, reduce, generateNumbers, isEqual, reverse, filter, forEach, remove
} = require('../list');

const checkListValues = (list, values) => {
    let iterationHead = list;
    let iterationIndex = 0;

    while (iterationHead !== EMPTY) {
        expect(iterationHead.value).toEqual(values[iterationIndex]);

        iterationIndex++;
        iterationHead = iterationHead.next;
    }
};

describe('List (sequence)', () => {
    describe('#create', () => {
        it('should make empty list', () => {
            const list = create();

            expect(list).toEqual(EMPTY);
        });

        it('should make list with predefined values', () => {
            const values = [1, 2, 3, 4];
            const list = create(...values);

            checkListValues(list, values);
        });
    });

    describe('#clone', () => {
        it('should clone empty list', () => {
            const list = clone();

            expect(list).toEqual(EMPTY);
        });

        it('should clone list with predefined values', () => {
            const values = [1, 2, 3, 4];
            const list = clone(create(...values));

            checkListValues(list, values);
        });
    });

    describe('#isEmpty', () => {
        it('should return true for empty list', () => {
            const list = create();

            expect(isEmpty(list)).toEqual(true);
        });

        it('should return false for non empty list', () => {
            const list = create(1, 2, 3, 4);

            expect(isEmpty(list)).toEqual(false);
        });
    });

    describe('#first', () => {
        it('should return empty value for empty list', () => {
            const list = create();

            expect(first(list)).toEqual(EMPTY);
        });

        it('should return first value for non empty list', () => {
            const list = create(1, 2, 3, 4);

            expect(first(list)).toEqual(1);
        });
    });

    describe('#rest', () => {
        it('should return empty value for empty list', () => {
            const list = create();

            expect(rest(list)).toEqual(EMPTY);
        });

        it('should return list without first node for non empty list', () => {
            const list = create(1, 2, 3, 4);

            checkListValues(rest(list), [2, 3, 4]);
        });

        it('should return empty list for list with one element', () => {
            const list = create(1);

            expect(rest(list)).toEqual(EMPTY);
        });
    });

    describe('#prefix', () => {
        it('should return list with element for empty list', () => {
            const list = create();

            checkListValues(prefix(list, 1), [1]);
        });

        it('should return list with element and list content for non empty list', () => {
            const list = create(1, 2, 3, 4);

            checkListValues(prefix(list, 0), [0, 1, 2, 3, 4]);
        });
    });

    describe('#postfix', () => {
        it('should return list with element for empty list', () => {
            const list = create();

            checkListValues(postfix(list, 1), [1]);
        });

        it('should return list with element and list content for non empty list', () => {
            const list = create(1, 2, 3, 4);

            checkListValues(postfix(list, 5), [1, 2, 3, 4, 5]);
        });
    });

    describe('#length', () => {
        it('should return 0 for empty list', () => {
            const list = create();

            expect(length(list)).toEqual(0);
        });

        it('should return actual non empty list length', () => {
            const list = create(1, 2, 3, 4);

            expect(length(list)).toEqual(4);
        });
    });

    describe('#elt', () => {
        it('should return empty for empty list', () => {
            const list = create();

            expect(elt(list, 1)).toEqual(EMPTY);
        });

        it('should return second element of list', () => {
            const list = create(1, 2, 3, 4);

            expect(elt(list, 2)).toEqual(2);
        });
    });

    describe('#cat', () => {
        it('should concat two empty lists', () => {
            expect(cat(create(), create())).toEqual(EMPTY);
        });

        it('should concat empty and non empty lists', () => {
            const list = create(1, 2, 3, 4);
            const emptyList = create();

            checkListValues(cat(emptyList, list), [1, 2, 3, 4]);
            checkListValues(cat(list, emptyList), [1, 2, 3, 4]);
        });

        it('should concat two non empty lists', () => {
            const list1 = create(1, 2, 3, 4);
            const list2 = create(5, 6);

            checkListValues(cat(list1, list2), [1, 2, 3, 4, 5, 6]);
        });
    });

    describe('#map', () => {
        it('should map empty list', () => {
            const emptyList = create();

            expect(map(emptyList, (v) => v)).toEqual(EMPTY);
        });

        it('should map non empty list', () => {
            const emptyList = create(1, 2, 3, 4);

            checkListValues(map(emptyList, v => v + 1), [2, 3, 4, 5]);
        });
    });

    describe('#reduce', () => {
        it('should reduce empty list', () => {
            const emptyList = create();

            expect(reduce(emptyList, (v) => v)).toEqual(EMPTY);
        });

        it('should reduce non empty list', () => {
            const emptyList = create(1, 2, 3, 4);

            expect(reduce(emptyList, (result, value) => result + value)).toEqual(10);
        });
    });

    describe('#generateNumbers', () => {
        it('should generate empty list if lower bound > upper bound', () => {
            expect(generateNumbers(3, 2)).toEqual(EMPTY);
        });

        it('should generate non empty list if lower bound < upper bound', () => {
            checkListValues(generateNumbers(1, 4), [1, 2, 3, 4]);
        });
    });

    describe('#isEqual', () => {
        it('should return true for two empty lists', () => {
            expect(isEqual(create(), create())).toEqual(true);
        });

        it('should return false if one of lists is empty', () => {
            expect(isEqual(create(), create(1, 2, 3, 4))).toEqual(false);
            expect(isEqual(create(1, 2, 3, 4), create())).toEqual(false);
        });

        it('should return true if lists have same elements', () => {
            expect(isEqual(create(1, 2, 3, 4), create(1, 2, 3, 4))).toEqual(true);
        });

        it('should return false if lists have different length', () => {
            expect(isEqual(create(1, 2, 3), create(1, 2, 3, 4))).toEqual(false);
        });
    });

    describe('#reverse', () => {
        it('should reverse empty list', () => {
            const list = reverse(create());

            checkListValues(list, []);
        });

        it('should reverse non empty list', () => {
            const list = reverse(create(1, 2, 3, 4));

            checkListValues(list, [4, 3, 2, 1]);
        });
    });

    describe('#filter', () => {
        it('should filter empty list', () => {
            expect(filter(create(), v => v)).toEqual(EMPTY);
        });

        it('should filter one element in list with one element', () => {
            expect(filter(create(1), v => v !== 1)).toEqual(EMPTY);
        });

        it('should filter some elements from list', () => {
            checkListValues(filter(create(1, 2, 3, 4), v => v % 2 === 0), [2, 4]);
        });
    });

    describe('#forEach', () => {
        it('should iterate on empty list', () => {
            const fn = jest.fn();

            forEach(create(), fn);
            expect(fn).not.toHaveBeenCalled();
        });

        it('should iterate on non empty list', () => {
            const fn = jest.fn();

            forEach(create(1, 2, 3, 4), fn);
            expect(fn.mock.calls.map(([arg]) => arg)).toEqual([1, 2, 3, 4]);
        });
    });

    describe('#remove', () => {
        it('should do nothing on empty list', () => {
            const list = create();

            checkListValues(remove(list, 1), []);
        });

        it('should remove element from non empty list', () => {
            const list = create(1, 2, 3, 4, 2);

            checkListValues(remove(list, 2), [1, 3, 4]);
        });
    });
});