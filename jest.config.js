'use strict';

module.exports = {
    testEnvironment: 'node',
    testRegex: '.*tests.js$',
    coverageThreshold: {
        global: {
            lines: 100,
            statements: 100,
            functions: 100,
            branches: 100
        }
    },
    coverageDirectory: './coverage',
    coverageReporters: [
        'lcov',
        'text',
        'html'
    ]
};
